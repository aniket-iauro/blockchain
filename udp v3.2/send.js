var udp = require('dgram');
var sock = udp.createSocket('udp4');

// emits when any error occurs
sock.on('error', function (error) {
    console.log('Error: ' + error);
    sock.close();
});

sock.on('message', function (msg, info) {
    if (msg.toString() === "new") {
        var call = "new";
        process.send(JSON.stringify({ call , info }));
    }
    else {
        var call = "call";
        var msg2 = msg.toString();
        process.send(JSON.stringify({ call, msg2, info }));
    }
});

//emits when socket is ready and listening for datagram msgs
function listen() {
    sock.on('listening', function () {
        var address = sock.address();
        console.log(address);
        var port = address.port;
        var family = address.family;
        var ipaddr = address.address;
        console.log('Server is listening at port' + port)
        console.log('Server ip :' + ipaddr);
        console.log('Server is IP4/IP6 : ' + family);
    });
}

//emits after the socket is closed using socket.close();
sock.on('close', function () {
    console.log('Socket is closed !');
});

sock.bind(2222, '192.168.43.174');

listen();
