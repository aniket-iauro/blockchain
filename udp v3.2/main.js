var udp = require('dgram');
var readlineSync = require("readline-sync");
const colors = require("colors");
const scanner = require('local-network-scanner');

// creating a socket
var sock = udp.createSocket('udp4');
var cli = udp.createSocket('udp4');
var recv, dev;

// emits when any error occurs
sock.on('error', function (error) {
  console.log('Error: ' + error);
  sock.close();
});

sock.on('message', function (msg, info) {
  var d = msg.toString();
  if (d === "new") {
    console.log('New member At %s:%d\n ', info.address, info.port);
  }
  else {
    d = JSON.parse(msg);
    if (d.timestamp) {
      console.log('Received %d bytes from %s:%d\n as /n  : %j', msg.length, info.address, info.port, d);
    }
    else {
      recv = d;
      console.log("msg from miner...");
    }
  }
});

//sending msg
function sendData() {

  var timestamp = readlineSync.question("\n\nEnter timestamp to send:".green);
  var data = readlineSync.question("\n\nEnter data to send:".green);

  for (var i = 0; i < dev.length; i++) {
    if (dev[i].ip !== "192.168.43.1") {
      cli.send(JSON.stringify({ timestamp, data }), '2222', dev[i].ip, function (error) {
        if (error) {
          console.log("error");
          sock.close();
        } else {
          console.log('Data sent !!!');
        }
      });
    }
  }
}

//emits when socket is ready and listening for datagram msgs
function listen() {
  sock.on('listening', function () {
    var address = sock.address();
    console.log(address);
    var port = address.port;
    var family = address.family;
    var ipaddr = address.address;
    console.log('Server is listening at port' + port)
    console.log('Server ip :' + ipaddr);
    console.log('Server is IP4/IP6 : ' + family);
  });
}

//emits after the socket is closed using socket.close();
sock.on('close', function () {
  console.log('Socket is closed !');
});

sock.bind(2222, '192.168.43.52');


function menu() {

  var lineread = readlineSync.question("\n\nEnter Option .........\n1.Print\n2.Send data\n3.Close:".green);

  switch (lineread) {
    case "1":
      console.log(JSON.stringify(recv, null, 4));
      menu();
      break;

    case "2":
      sendData();
      menu();
      break;

    case "3":
      console.log("close");
      break;

    default:
      setTimeout(function () {
        menu()
      }, 0);
      break;
  }
}

//Initial
scanner.scan(devices => {
  dev = devices;
});
listen();

setTimeout(function () {
  console.log(dev);
  for (var i = 0; i < dev.length; i++) {
    if (dev[i].ip !== "192.168.43.1") {
      cli.send("new", '2222', dev[i].ip, function (error) {
        if (error) {
          console.log("error");
          sock.close();
        } else {
          console.log('First Data sent !!!');
        }
      });
    }
  }
  menu();
}, 2000);
