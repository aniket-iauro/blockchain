var udp = require('dgram');
var readlineSync = require("readline-sync");
const colors = require("colors");
const sha256 = require('crypto-js/sha256');
const cp = require('child_process');
const scanner = require('local-network-scanner');

var child = cp.fork("send.js");

child.on("exit", () => {
    console.log("Child terminated");
})

class block {
    constructor(index, timestamp, data, previoushash) {
        this.index = index;
        this.timestamp = timestamp;
        this.data = data;
        this.previoushash = '';
        this.hash = this.calculatehash();
    }
    calculatehash() {
        return sha256(this.index + this.previoushash + this.timestamp + JSON.stringify(this.data)).toString();
    }
}

class blockchain {

    constructor() {
        this.chain = [this.createGenesisblock()];
    }

    adddata(data) {
        //console.log(data);
        this.chain.push(data[data.length - 1]);
    }

    createGenesisblock() {
        return new block(0, "23/7/18", "genesis block", "0");
    }

    getlatestblock() {
        return this.chain[this.chain.length - 1];
    }

    addblock(newblock) {
        newblock.previoushash = this.getlatestblock().hash;
        newblock.hash = newblock.calculatehash();
        this.chain.push(newblock);
    }

    ischainvalid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentblock = this.chain[i];
            const previousblock = this.chain[i - 1];

            if (currentblock.hash !== currentblock.calculatehash()) {
                return false;
            }

            if (currentblock.previoushash !== previousblock.hash) {
                return false;
            }
        }
        return true;
    }
}

let coin = new blockchain(), i = 1;
var dev;

child.on("message", (data) => {
    var d = JSON.parse(data);

    if (d.call === "new") {
        cli.send(JSON.stringify(coin), '2222', d.info.address, function (error) {
            if (error) {
                console.log("error");
                sock.close();
            } else {
                console.log('legder sent to new client !!!');
            }
            setTimeout(function () {
                menu();
            }, 5000);
        })
    }

    if (d.call === "call")
        recvData(d.msg2, d.info);
})

// creating a socket
var cli = udp.createSocket('udp4');

// emits on new datagram msg
function recvData(msg, info) {
    var d = JSON.parse(msg);
    console.log('Received %d bytes from %s:%d\n as /n  : %j', msg.length, info.address, info.port, d);
    coin.addblock(new block(i++, d.timestamp, d.data));

    for (var i = 0; i < dev.length; i++) {
        if (dev[i].ip !== "192.168.43.1") {
            cli.send(JSON.stringify(coin), '2222', dev[i].ip, function (error) {
                if (error) {
                    console.log("error");
                    sock.close();
                } else {
                    console.log('Added new ledger and send !!!');
                }
            })
        }
    }

    // console.log("computing");   
    setTimeout(function () {
        menu();
    }, 5000);
}

//sending msg
function sendData() {
    var timestamp = readlineSync.question("\n\nEnter timestamp to send:".green);
    var data = readlineSync.question("\n\nEnter data to send:".green);
    coin.addblock(new block(i++, timestamp, data));

    for (var i = 0; i < dev.length; i++) {
        if (dev[i].ip !== "192.168.43.1") {
            cli.send(JSON.stringify(coin), '2222', dev[i].ip, function (error) {
                if (error) {
                    console.log("error");
                    sock.close();
                } else {
                    console.log('Data send !!!');
                }
            })
        }
    };
}

//main menu
function menu() {

    var lineread = readlineSync.question("\n\nEnter Option .........\n1.Print\n2.Send data\n3.Close:".green);
    switch (lineread) {
        case "1":
            console.log(JSON.stringify(coin, null, 4).blue);
            setTimeout(function () {
                menu();
            }, 0);
            break;

        case "2":
            sendData();
            menu();
            break;

        case "3":
            console.log("close");
            break;

        default:
            setTimeout(function () {
                //recvData();
                menu();
            }, 0);
            break;
    }
}

scanner.scan(devices => {
    dev = devices;
});

setTimeout(function () {
    console.log(dev);
    menu();
}, 2000);